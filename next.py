#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Find the next train to a given destination"""
import hashlib
import pickle
import time
from collections import defaultdict
from datetime import datetime, date
from pathlib import Path

import requests

from _keys import APP_ID, APP_KEY

URL = (
    "https://transportapi.com/v3/uk/train/station/{station}/live.json?"
    "app_id={id}&"
    "app_key={key}&"
    "calling_at={calling_at}&"
    "darwin=true&"
    "train_status=passenger"
)
UTF8 = "utf-8"


class Train:
    INFO = [
        "platform",
        "aimed_departure_time",
        "expected_departure_time",
        "best_departure_estimate_mins",
        "destination_name",
        "status",
    ]

    def __init__(self, info):
        self._info = defaultdict(lambda: None, info)

    @classmethod
    def has_info(cls, info):
        """Check a dictionary has all the required info

        :param info: The attribute to check

        >>> d = {k:1 for k in Train.INFO}
        >>> Train.has_info(d)
        True
        >>> del d["status"]
        >>> Train.has_info(d)
        False
        """
        return all(i in info for i in cls.INFO)

    def __getattr__(self, attr):
        """Expose attributes in self._info

        :param attr: an attribute from Train.INFO

        >>> t = Train({"status": 1, "bad_val": 2})
        >>> t.status
        1
        >>> t.bad_val
        Traceback (most recent call last):
        ...
        AttributeError: Train has no attribute bad_val
        >>> t.destination_name

        >>> t.made_up
        Traceback (most recent call last):
        ...
        AttributeError: Train has no attribute made_up
        """
        if attr not in self.INFO:
            raise AttributeError(f"{self.__class__.__name__} has no attribute {attr}")

        return self._info[attr]

    @property
    def expected_time(self):
        """The the expected time the train will leave

        >>> t = Train({"expected_departure_time": "12:22", "aimed_departure_time": "12:24"})
        >>> t.expected_time
        '12:22'
        >>> t = Train({"expected_departure_time": "", "aimed_departure_time": "12:24"})
        >>> t.expected_time
        '12:24'
        >>> t = Train({"aimed_departure_time": "12:24"})
        >>> t.expected_time
        '12:24'
        """
        return self.expected_departure_time or self.aimed_departure_time

    def time_to_train(self, from_time):
        """The time (in minutes) from 'from_time' until the train departs

        :param from_time: a datetime.time

        >>> from datetime import time
        >>> t = Train({"expected_departure_time": "16:22"})
        >>> t.time_to_train(time(16, 17, 33))
        4
        >>> t.time_to_train(time(14, 55, 58))
        86
        """
        train_time = datetime.strptime(self.expected_time, "%H:%M").time()
        return int(
            (
                datetime.combine(date.today(), train_time)
                - datetime.combine(date.today(), from_time)
            ).total_seconds()
            / 60
        )

    def __str__(self):
        """ A string representation of a departing train

        >>> td = {}
        >>> td["aimed_departure_time"] = "23:20"
        >>> td["destination_name"] = "Kings Cross"
        >>> td["platform"] = "9 3/4"
        >>> t = Train(td)
        >>> str(t)
        '23:20 Kings Cross [P9 3/4] - ... mins'
        """
        return (
            f"{self.aimed_departure_time} {self.destination_name} "
            f"[P{self.platform}] - "
            f"{self.time_to_train(datetime.now().time())} mins"
        )


class QueryTool:
    """A query manager for caching results in a file"""

    QUERY_FILE = "train-{0}.pkl"
    QUERY_DIR = Path("/tmp")
    QUERY_INTERVAL = 30

    def _query_path(self, query):
        """The name of a file to store the results of a specific query

        :param: an UTF-8 encoded string

        >>> QueryTool()._query_path("http://www.google.com".encode(UTF8))
        PosixPath('/tmp/train-738ddf35b3a85a7a6ba7b232bd3d5f1e4d284ad1.pkl')
        """
        query_hash = hashlib.sha1(query).hexdigest()
        return self.QUERY_DIR / self.QUERY_FILE.format(query_hash)

    def query(self, query):
        self._tidy_old_queries()
        if self._use_existing_query(query):
            return self._load_query(query)
        return self._new_query(query)

    def _tidy_old_queries(self):
        for path in self.QUERY_DIR.glob(self.QUERY_FILE.format("*")):
            if _path_age(path) > self.QUERY_INTERVAL:
                path.unlink()

    def _use_existing_query(self, query):
        """Determine whether to use results from a previous query

        >>> import tempfile
        >>> class QT(QueryTool): pass
        >>> with tempfile.NamedTemporaryFile() as tmpfile:
        ...    QT._query_path = lambda a, b: Path(tmpfile.name)
        ...    QT()._use_existing_query("dummy")
        True
        >>> with tempfile.NamedTemporaryFile() as tmpfile:
        ...    QT._query_path = lambda a, b: Path(tmpfile.name)
        ...    QT.QUERY_INTERVAL = 0
        ...    QT()._use_existing_query("dummy")
        False
        >>> QT()._use_existing_query("dummy")
        False
        """
        path = self._query_path(query)
        return path.exists() and _path_age(path) < self.QUERY_INTERVAL

    def _new_query(self, query):
        """

        """
        resp = requests.get(query)
        if not resp.ok:
            return {}

        result = resp.json()
        query_name = self._query_path(query)
        with open(query_name, "wb") as query_file:
            pickle.dump(result, query_file)

        return result

    def _load_query(self, query):
        """Load the results from a previous query

        >>> import tempfile
        >>> class QT(QueryTool): pass
        >>> with tempfile.NamedTemporaryFile() as tmpfile:
        ...    QT._query_path = lambda a, b: Path(tmpfile.name)
        ...    pickle.dump("SENTINAL", tmpfile)
        ...    _ = tmpfile.seek(0)
        ...    QT()._load_query("dummy")
        'SENTINAL'
        """
        query_name = self._query_path(query)
        with open(query_name, "rb") as query_file:
            return pickle.load(query_file)


def departures_query(station, calling_at):
    """Get a JSON blob showing all departures from a station calling at another

    Stations are specified as CRS or TIPLOC http://www.railwaycodes.org.uk/crs/CRS0.shtm

    :param station: The station the train is departing
    :param calling_at: A station the train should call at
    """
    url = URL.format(
        id=APP_ID, key=APP_KEY, station=station, calling_at=calling_at
    ).encode(UTF8)
    return QueryTool().query(url)["departures"]["all"]


def _path_age(path):
    """The current age of a Path"""
    return time.time() - path.stat().st_mtime


def trains(depatures):
    """Convert a JSON blob of departures into sorted Train objects

    >>> blob = [{}, {}]
    >>> blob[0]["aimed_departure_time"] = "23:20"
    >>> blob[0]["destination_name"] = "Kings Cross"
    >>> blob[0]["platform"] = "9 3/4"
    >>> blob[1]["aimed_departure_time"] = "22:50"
    >>> blob[1]["destination_name"] = "Kings Cross"
    >>> blob[1]["platform"] = "11"
    >>> trains = trains(blob)
    >>> str(trains[0])
    '22:50 Kings Cross [P11] - ... mins'
    >>> str(trains[1])
    '23:20 Kings Cross [P9 3/4] - ... mins'
    """
    return sorted(
        list(Train(d) for d in depatures),
        key=lambda x: x.time_to_train(datetime.now().time()),
    )


def latest_train(*args, **kwargs):
    return str(trains(departures_query("CBG", "RYS"))[0] or "No train info")


if __name__ == "__main__":
    print(latest_train())
